﻿using Microsoft.AspNetCore.Http;
using System;
using System.DirectoryServices;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using DragonFlyAdmin.Interfaces;

namespace DragonFlyAdmin.Services
{
    /// <summary>
    /// This is the AD Service that will handle any AD related tasks
    /// </summary>
    public class AdService : ILoggerInterface
    {
        private readonly ClaimsPrincipal _claim;
        private readonly IHttpContextAccessor _accessor;
        private readonly LoggerService _loggerService;
        /// <summary>
        /// Default constructor that creates any additional services it might need
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public AdService(IHttpContextAccessor httpContextAccessor)
        {
            _accessor = httpContextAccessor;
            _claim = httpContextAccessor.HttpContext?.User;
            _loggerService = CreateLogger();
        }
        /// <summary>
        /// Returns the IPrincipal for the logged in user
        /// </summary>
        public IPrincipal CurrentUser
        {
            get
            {
                var context = _accessor.HttpContext;
                return context != null ? context.User : ClaimsPrincipal.Current;
            }
        }
        /// <summary>
        /// Determines if the logged in user is in the role specified
        /// </summary>
        /// <param name="role">Role</param>
        /// <returns>bool (yes/no) in role</returns>
        public bool IsInRole(string role)
        {
            var isInRole = false;
            var user = CurrentUser;
            if (user != null)
            {
                isInRole = user.IsInRole(role);
            }
            return isInRole;
        }
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("AdService");
            return logger;
        }
        /// <summary>
        /// Removes domain and slashes from username
        /// </summary>
        /// <param name="user">Logged in user</param>
        /// <returns></returns>
        public string CleanUsername(string user)
        {
            try
            {
                var findSlash = user.IndexOf("\\", StringComparison.Ordinal);
                user = user.Substring(findSlash + 1);
                return user.ToUpper();
            }
            catch (Exception e)
            {
                // log eventually
                return null;
            }

        }
        /// <summary>
        /// Determines if logged in user is a Member
        /// </summary>
        /// <returns>true/false</returns>
        public bool IsMember()
        {
            return IsInRole("Dragon fly Member");
        }
        /// <summary>
        /// Determines if logged in user is an Admin
        /// </summary>
        /// <returns>true/false</returns>
        public bool IsAdmin()
        {
            return IsInRole("BHSQL Web Team Developers") || IsInRole("Dragon fly Admin");
        }
        /// <summary>
        /// Gets logged in user
        /// </summary>
        /// <returns>username</returns>
        public string GetUserName()
        {
            try
            {
                return _accessor.HttpContext?.User.Identity?.Name;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetUserName occurred :: Details :: " + e.Message + " " + e.InnerException ?? "");
                throw;
            }
        }
        /// <summary>
        /// Get full name for user
        /// </summary>
        /// <param name="userId">logged in user's user id</param>
        /// <returns></returns>
        public string GetFullName(string userId)
        {
            try
            {
                var filter = new StringBuilder();
                filter.Append($"(sAMAccountName={userId})");
                var search = new DirectorySearcher(new DirectoryEntry())
                {
                    PageSize = 1000,
                    Filter = $"(&(objectClass=user)(objectCategory=person)(|{filter}))"
                };
                search.PropertiesToLoad.Add("displayname");
                search.PropertiesToLoad.Add("givenName");

                var result = search.FindOne();
                var managerName = GetProperty(result, "displayname");
                return managerName;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: IsUserPermitted :: UserID: " + userId + " occurred :: Details :: " + e.Message + " " + e.InnerException ?? "");
                throw;
            }

        }
        /// <summary>
        /// This pulls out the actual value from the AD property passed in
        /// </summary>
        /// <param name="searchResult">Search Result passed in that contains the properties specified</param>
        /// <param name="propertyName">Property searching</param>
        /// <returns></returns>
        public string GetProperty(SearchResult searchResult, string propertyName)
        {
            try
            {
                return searchResult.Properties.Contains(propertyName)
                    ? searchResult.Properties[propertyName][0].ToString()
                    : string.Empty;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetProperty occurred :: Details :: " + e.Message + " " +
                    e.InnerException ?? "");
                throw;
            }
        }
        
    }
}
