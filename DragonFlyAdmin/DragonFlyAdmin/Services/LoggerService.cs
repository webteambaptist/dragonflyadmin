﻿using System;
using NLog;

namespace DragonFlyAdmin.Services
{
    /// <summary>
    /// This is the logger service that handles all logging activies
    /// </summary>
    public class LoggerService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Constructor used to construct and instance of the logger
        /// </summary>
        /// <param name="fileName">The name of the Log to be created</param>
        public LoggerService(string fileName)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var datetime = DateTime.Now.Date.ToString("MM-dd-yy");
            var logFileName = $"Logs\\{fileName}{datetime}.log";
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = logFileName };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
        }
        /// <summary>
        /// Writes Information to log
        /// </summary>
        /// <param name="text">text to be logged</param>
        public void WriteInfo(string text)
        {
            _logger.Info(text);
        }
        /// <summary>
        /// Writes Error to log
        /// </summary>
        /// <param name="text">text to be logged</param>
        public void WriteError(string text)
        {
            _logger.Error(text);
        }
    }
}
