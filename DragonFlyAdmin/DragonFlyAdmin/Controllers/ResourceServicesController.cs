﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.ResourceServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all the add/remove of Services for a Resource
    /// </summary>
    public class ResourceServicesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Main constructor that builds services needed
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ResourceServicesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        // GET: Resource/ResourceServicesIndex/ResourceId
        /// <summary>
        /// Loads the Resource Services View
        /// </summary>
        /// <param name="id">resource Id</param>
        /// <returns>List of Resource Services</returns>
        [Route("ResourceServices/Index/{id}")]
        [HttpGet("Index/{id}")]
        public async Task<IActionResult> Index(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var model = await BuildResourceServiceIndexModel(id.Value);
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceService::Index({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the model for the Resource Service View
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>Model </returns>
        private async Task<ResourceServiceModel> BuildResourceServiceIndexModel(int resourceId)
        {
            try
            {
                var model = new ResourceServiceModel
                {
                    ServiceModelList = new List<ServiceModel>(),
                    ResourceId = resourceId
                };
                // build drop down of locations for adding
                var services = await _context.Services.ToListAsync();
                var listOptions = services.Select(service => new SelectListItem(service.Service1, service.Id.ToString())).ToList();
                model.ServiceList = listOptions;

                // build LocationList
                var providerServices = await _context.ResourceServices.Where(x => x.ResourceId == resourceId).ToListAsync();
                foreach (var pl in providerServices)
                {
                    var service = await _context.Services.Where(x => x.Id == pl.ServicesId).FirstOrDefaultAsync();
                    if (service != null)
                    {
                        var iModel = new ServiceModel()
                        {
                            EntryUser = pl.EntryUser,
                            EntryDt = pl.EntryDt,
                            UpdateUser = pl.UpdateUser,
                            UpdateDt = pl.UpdateDt,
                            ResourceId = pl.ResourceId,
                            ServiceId = service.Id,
                            Service = service.Service1
                        };

                        model.ServiceModelList.Add(iModel);
                    }

                    model.IsAdmin = _adService.IsAdmin();
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceServices::BuildResourceServiceIndexModel({resourceId}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Adds the new Resource Service
        /// </summary>
        /// <param name="model">Form Model</param>
        /// <returns>Back to resource list</returns>
        [Route("ResourceServices/Create")]
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ResourceServiceModel model)
        {
            try
            {
                var resourceId = model.ResourceId;
                var serviceId = int.Parse(model.SelectedService);

                var resourceService = new ResourceService()
                {
                    ResourceId = resourceId,
                    ServicesId = serviceId,
                    EntryUser = _userId
                };
                if (_context.ResourceServices.Any(x => x.ServicesId == serviceId && x.ResourceId == resourceId))
                {
                    ViewData["Unique"] =
                        $"Resource already has this Service {await _context.ResourceServices.Where(x => x.Id == serviceId).Select(x => x.Services).FirstAsync()}";
                    return View("Index", await BuildResourceServiceIndexModel(model.ResourceId));
                }
                _context.ResourceServices.Add(resourceService);
                await _context.SaveChangesAsync();

                // build new model for display
                var viewModel = await BuildResourceServiceIndexModel(model.ResourceId);
                return View("Index", viewModel);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: :: ResourceServices::Create({JsonConvert.SerializeObject(model)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // Get: Resource/DeleteResourceService/5/6
        /// <summary>
        /// Loads the Delete Confirmation page
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="serviceId">Service Id</param>
        /// <returns>Delete Confirmation page</returns>
        [Route("ResourceServices/Delete/{resourceId}/{serviceId}")]
        [HttpGet("Delete/{resourceId}/{serviceId}")]
        public async Task<IActionResult> Delete(int? resourceId, int? serviceId)
        {
            try
            {
                if (resourceId == null || serviceId == null)
                {
                    return NotFound();
                }

                var service = await _context.Services.Where(x => x.Id == serviceId).FirstOrDefaultAsync();
                // build model for View
                if (service == null) return NotFound();
                {
                    var model = new ResourceServiceDeleteModel
                    {
                        ServiceId = serviceId.Value,
                        ResourceId = resourceId.Value,
                        ResourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                            .FirstOrDefaultAsync(),
                        ServiceName = service.Service1,
                        IsAdmin = _adService.IsAdmin()
                    };
                    return View(model);
                }

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceServices::Delete({resourceId}:{serviceId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // POST: Resource/Delete/5
        /// <summary>
        /// Logs the original Resource Service and then deletes it
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="serviceId">Service Id</param>
        /// <returns></returns>
        [Route("ResourceServices/DeleteConfirmed/{resourceId}/{serviceId}")]
        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int resourceId, int serviceId)
        {
            try
            {
                var providerService = await _context.ResourceServices.Where(x => x.ResourceId == resourceId && x.ServicesId == serviceId).FirstOrDefaultAsync();
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(providerService)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                if (providerService != null) _context.ResourceServices.Remove(providerService);
                await _context.SaveChangesAsync();
                var resourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                    .FirstOrDefaultAsync();
                ViewData["Success"] = $"Age successfully removed from Resource {resourceName}";
                return RedirectToAction("Index", "ResourceServices", new {id=resourceId});
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceServices::DeleteConfirmed({resourceId}:{serviceId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Create an instance of LoggerService
        /// </summary>
        /// <returns>instance of logger service</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("ResourceServices");
        }
    }
}
