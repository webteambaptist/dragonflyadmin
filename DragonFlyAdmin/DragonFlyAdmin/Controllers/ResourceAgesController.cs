﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.ResourceAges;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all the add/remove of Resource Ages
    /// </summary>
    public class ResourceAgesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default constructor that creates instances of services used
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ResourceAgesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        // GET: Resource/Index/ResourceId
        /// <summary>
        /// Displays the list of Ages for a Resources
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>Resource Ages View</returns>
        [Route("ResourceAges/Index/{id}")]
        [HttpGet("Index/{id}")]
        public async Task<IActionResult> Index(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var model = await BuildResourceAgeIndexModel(id.Value);
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceAgeIndex ({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the model for the Resource Ages View
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>model</returns>
        private async Task<ResourceAgeModel> BuildResourceAgeIndexModel(int resourceId)
        {
            try
            {
                var model = new ResourceAgeModel()
                {
                    ResourceAgesList = new List<AgeModel>(),
                    ResourceId = resourceId
                };
                // build drop down of locations for adding
                var ages = await _context.Ages.ToListAsync();
                var listOptions = ages.OrderBy(x => x.Age1).Select(ageRange => new SelectListItem(ageRange.Age1, ageRange.Id.ToString())).ToList();
                model.AgeList = listOptions;

                // build  ageList
                var resourceAges = await _context.ResourceAges.Where(x => x.ResourceId == resourceId).ToListAsync();
                foreach (var pa in resourceAges)
                {
                    var age = await _context.Ages.Where(x => x.Id == pa.AgeId).FirstOrDefaultAsync();
                    var iModel = new AgeModel()
                    {
                        EntryUser = pa.EntryUser,
                        EntryDt = pa.EntryDt,
                        UpdateUser = pa.UpdateUser,
                        UpdateDt = pa.UpdateDt,
                        ResourceId = pa.ResourceId,
                        AgeId = age.Id,
                        Age = age.Age1
                    };

                    model.ResourceAgesList.Add(iModel);
                    model.IsAdmin = _adService.IsAdmin();
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceAges::BuildResourceAgeIndexModel({resourceId}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Adds a new Resource Age
        /// </summary>
        /// <param name="model">Age from form</param>
        /// <returns>Back to the Resource List</returns>
        [Route("ResourceAges/Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ResourceAgeModel model)
        {
            try
            {
                var resourceId = model.ResourceId;
                var ageId = int.Parse(model.SelectedAge);

                var resourceAge = new ResourceAge()
                {
                    AgeId = ageId,
                    ResourceId = resourceId,
                    EntryUser = _userId,
                    EntryDt = DateTime.Now
                };
                if (_context.ResourceAges.Any(x => x.AgeId == ageId && x.ResourceId == resourceId))
                {
                    ViewData["Unique"] =
                        $"Resource already has this Age {await _context.ResourceAges.Where(x => x.Id == ageId).Select(x => x.Age).FirstAsync()}";
                    return View("Index", await BuildResourceAgeIndexModel(model.ResourceId));
                }
                _context.ResourceAges.Add(resourceAge);
                await _context.SaveChangesAsync();

                // build new model for display
                return View("Index", await BuildResourceAgeIndexModel(model.ResourceId));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceAges::Create({JsonConvert.SerializeObject(model)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // Get: Resource/DeleteResourceAge/5/6
        /// <summary>
        /// Gets the Delete Confirmation page
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="ageId">Age Id</param>
        /// <returns>Delete Confirmation page</returns>
        [Route("ResourceAges/Delete/{resourceId}/{ageId}")]
        [HttpGet("Delete/{resourceId}/{ageId}")]
        public async Task<IActionResult> Delete(int? resourceId, int? ageId)
        {
            try
            {
                if (resourceId == null || ageId == null)
                {
                    return NotFound();
                }

                var age = await _context.Ages.Where(x => x.Id == ageId).FirstOrDefaultAsync();
                // build model for View
                if (age == null) return NotFound();
                {
                    var model = new ResourceAgesDeleteModel()
                    {
                        AgeId = ageId.Value,
                        ResourceId = resourceId.Value,
                        ResourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                            .FirstOrDefaultAsync(),
                        AgeName = age.Age1,
                        IsAdmin = _adService.IsAdmin()
                    };
                    return View(model);
                }

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceAges::Delete({resourceId}:{ageId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // POST: Resource/DeleteResourceAgeConfirmed/5
        /// <summary>
        /// Logs original ResourceAge and then deletes it
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="ageId">Age Id</param>
        /// <returns>Back to the Resource List</returns>
        [Route("ResourceAges/DeleteConfirmed/{resourceId}/{ageId}")]
        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int resourceId, int ageId)
        {
            try
            {
                var resourceAge = await _context.ResourceAges.Where(x => x.ResourceId == resourceId && x.AgeId == ageId).FirstOrDefaultAsync();
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(resourceAge)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                _context.ResourceAges.Remove(resourceAge);
                await _context.SaveChangesAsync();
                var resourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                    .FirstOrDefaultAsync();
                ViewData["Success"] = $"Age successfully removed from Resource {resourceName}";
                return RedirectToAction("Index", "ResourceAges", new {id=resourceId});
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceAges::DeleteConfirmed({resourceId}:{ageId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>Instance of LoggerService</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("ResourceAges");
        }
    }
}
