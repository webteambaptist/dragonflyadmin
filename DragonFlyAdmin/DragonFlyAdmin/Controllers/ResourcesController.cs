﻿using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.Resources;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove/edit of Resources
    /// </summary>
    public class ResourcesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default constructor that creates instances of all services needed
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ResourcesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Displays the Index page
        /// </summary>
        /// <returns>list of resources</returns>
        [HttpGet("Index")]
        public async Task<IActionResult> Index()
        {
            try
            {
                var resources = await _context.Resources.OrderBy(x => x.Name).ToListAsync();
                var model = new ResourceIndexModel()
                {
                    ResourceList = BuildResourceViews(resources),
                    IsAdmin = _adService.IsAdmin()
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::Index() :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the Resources List View Model from DB Model List
        /// </summary>
        /// <param name="resourceList">db list of resources</param>
        /// <returns>vm list of resources</returns>
        private List<ResourceView> BuildResourceViews(IEnumerable<Resource> resourceList)
        {
            return resourceList.Select(resource => new ResourceView
            {
                Id = resource.Id,
                Name = resource.Name,
                PhoneNumber = resource.PhoneNumber,
                Website = resource.Website,
                EntryDt = resource.EntryDt,
                EntryUser = resource.EntryUser,
                UpdateDt = resource.UpdateDt,
                UpdateUser = resource.UpdateUser
            })
                .ToList();
        }
        /// <summary>
        /// Builds the vm resource from db resource
        /// </summary>
        /// <param name="resource">db resource</param>
        /// <returns>vm resource</returns>
        private ResourceView BuildResourceView(Resource resource)
        {
            return new ResourceView
            {
                Id = resource.Id,
                Name = resource.Name,
                PhoneNumber = resource.PhoneNumber,
                Website = resource.Website,
                EntryDt = resource.EntryDt,
                EntryUser = resource.EntryUser,
                UpdateDt = resource.UpdateDt,
                UpdateUser = resource.UpdateUser
            };
        }
        /// <summary>
        /// Displays the Details View for a Resource
        /// </summary>
        /// <param name="id">resource id</param>
        /// <returns>Details View</returns>
        [HttpGet("Details/{id}")]
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var resource = await _context.Resources.FirstOrDefaultAsync(m => m.Id == id);
                if (resource == null)
                {
                    return NotFound();
                }

                return View(BuildResourceView(resource));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::Details({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Displays the create View
        /// </summary>
        /// <returns>Create View</returns>
        [HttpGet("Create")]
        public IActionResult Create()
        {
            var resource = new ResourceEditModel
            {
                IsAdmin = _adService.IsAdmin(),
                IsMember = _adService.IsMember(),
                EntryDt = DateTime.Now,
                EntryUser = _userId
            };

            return View(resource);
        }
        /// <summary>
        /// This Adds a new Provider to the Database
        /// </summary>
        /// <param name="resourceModel">Resource from Form</param>
        /// <returns>Back to the Provider List</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateResource(ResourceEditModel resourceModel)
        {
            try
            {
                var resource = new Resource
                {
                    EntryUser = resourceModel.EntryUser,
                    Name = resourceModel.Name,
                    PhoneNumber = resourceModel.PhoneNumber,
                    Website = resourceModel.Website,
                    EntryDt = resourceModel.EntryDt
                };

                if (!ModelState.IsValid) return View("Create", BuildResourceView(resource));
                if (_context.Resources.Any(x => x.Name == resourceModel.Name))
                {
                    ViewData["Unique"] = $"Resource '{resourceModel.Name}' already exists.";
                    return View("Create", BuildResourceView(resource));
                }
                _context.Add(resource);
                await _context.SaveChangesAsync();
                ViewData["Success"] = $"{resourceModel.Name} Successfully Created!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::Create({JsonConvert.SerializeObject(resourceModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Displays the Edit page for a Resource
        /// </summary>
        /// <param name="id">resource id</param>
        /// <returns>Edit View</returns>

        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var resource = await _context.Resources.FindAsync(id);
                if (resource == null)
                {
                    return NotFound();
                }
                resource.UpdateDt = DateTime.Now;
                resource.UpdateUser = _userId;
                return View(BuildResourceEditModel(resource));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::Edit({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Saves the updated Resource
        /// </summary>
        /// <param name="id">Resource id</param>
        /// <param name="resourceModel">form</param>
        /// <returns>Resources Index</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,PhoneNumber,Website,EntryUser,EntryDt,UpdateUser,UpdateDt")] ResourceEditModel resourceModel)
        {
            try
            {
                if (id != resourceModel.Id)
                {
                    return NotFound();
                }
                var resource = await _context.Resources.Where(x => x.Id == id).FirstAsync();
                resource.UpdateUser = resourceModel.UpdateUser;
                resource.UpdateDt = DateTime.Now;
                resource.Name = resourceModel.Name;
                resource.PhoneNumber = resourceModel.PhoneNumber;
                resource.Website = resourceModel.Website;

                if (!ModelState.IsValid) return View(BuildResourceEditModel(resource));


                // if name changed and exists already
                if (resource.Name != resourceModel.Name && _context.Resources.Any(x => x.Name == resourceModel.Name))
                {
                    ViewData["Unique"] = $"Resource '{resource.Name}' already exists.";
                    return View(BuildResourceEditModel(resource));
                }
                try
                {
                    _context.Update(resource);
                    await _context.SaveChangesAsync();
                    ViewData["Success"] = $"{resource.Name} Successfully Updated!";
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!ResourceExists(resourceModel.Id))
                    {
                        _logger.WriteError($"DBException :: Not Found :: Resources::Edit({id}:{JsonConvert.SerializeObject(resourceModel)}) :: {ex.Message}");
                        return NotFound();
                    }
                    _logger.WriteError($"DBException :: Resources::Edit({id}:{JsonConvert.SerializeObject(resourceModel)}) :: {ex.Message}");
                    return RedirectToAction("Error", "Home");
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::Edit({id}:{JsonConvert.SerializeObject(resourceModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        // GET: Resources/Delete/5
        /// <summary>
        /// Initiates a Delete of a Selected Resource
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>Delete Confirmation View</returns>
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var resource = await _context.Resources
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (resource == null)
                {
                    return NotFound();
                }

                return View(BuildResourceEditModel(resource));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::Delete ({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Saves the Resource to DeleteLog and then removes from db
        /// </summary>
        /// <param name="id">resource id</param>
        /// <returns>Resources Index</returns>
        [Route("Resources/DeleteConfirmed/{id}")]
        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var resource = await _context.Resources.FindAsync(id);
                var log = new DeleteLog()
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(resource)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                _context.Resources.Remove(resource);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Resources::DeleteConfirmed({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Checks if resource exists in the db
        /// </summary>
        /// <param name="id">resource id</param>
        /// <returns>true/false</returns>
        private bool ResourceExists(int id)
        {
            return _context.Resources.Any(e => e.Id == id);
        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>instance of logger service</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Resources");
        }
        /// <summary>
        /// This Builds a model for the Resource Edit 
        /// </summary>
        /// <param name="resource">Given Resource</param>
        /// <returns>ProviderEditModel</returns>
        private ResourceEditModel BuildResourceEditModel(Resource resource)
        {
            var model = new ResourceEditModel
            {
                Id = resource.Id,
                Name = resource.Name,
                PhoneNumber = resource.PhoneNumber,
                Website = resource.Website,
                EntryDt = resource.EntryDt,
                EntryUser = resource.EntryUser,
                UpdateDt = resource.UpdateDt,
                UpdateUser = resource.UpdateUser,
                IsAdmin = _adService.IsAdmin(),
                IsMember = _adService.IsMember()
            };
            return model;
        }
    }
}
