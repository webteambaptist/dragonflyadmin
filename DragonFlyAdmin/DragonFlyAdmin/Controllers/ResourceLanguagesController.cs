﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.ResourceLanguages;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all the add/remove of ResourceLanguages
    /// </summary>
    public class ResourceLanguagesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default constructor that builds services needed
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ResourceLanguagesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        // GET: Resource/Index/ResourceId
        /// <summary>
        /// Loads the Resource Language view
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>List of Resource Langauges</returns>
        [Route("ResourceLanguages/Index/{id}")]
        [HttpGet("Index/{id}")]
        public async Task<IActionResult> Index(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var model = await BuildResourceLanguageIndexModel(id.Value);
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLanguages::Index({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the model for the Resource Languages View
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <returns>Resource language model</returns>
        private async Task<ResourceLanguageModel> BuildResourceLanguageIndexModel(int resourceId)
        {
            try
            {
                var model = new ResourceLanguageModel()
                {
                    LanguageModelList = new List<LanguageModel>(),
                    ResourceId = resourceId
                };
                // build drop down of languages for adding
                var languages = await _context.Languages.OrderBy(x=>x.Language1).ToListAsync();
                var listOptions = languages.Select(language => new SelectListItem(language.Language1, language.Id.ToString())).ToList();
                model.LanguageList = listOptions;

                // build languageList
                var resourceSpecialties = await _context.ResourceLanguages.Where(x => x.ResourceId == resourceId).ToListAsync();
                foreach (var pl in resourceSpecialties)
                {
                    var language = await _context.Languages.Where(x => x.Id == pl.LanguageId).FirstOrDefaultAsync();
                    var iModel = new LanguageModel()
                    {
                        EntryUser = pl.EntryUser,
                        EntryDt = pl.EntryDt,
                        UpdateUser = pl.UpdateUser,
                        UpdateDt = pl.UpdateDt,
                        ResourceId = pl.ResourceId,
                        Language = language.Language1,
                        LanguageId = pl.LanguageId
                    };

                    model.LanguageModelList.Add(iModel);
                    model.IsAdmin = _adService.IsAdmin();
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLanguages::BuildResourceLanguageIndexModel({resourceId}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Adds the language for the resource
        /// </summary>
        /// <param name="model">Model from form</param>
        /// <returns>Back to the Resource list</returns>
        [Route("ResourceLanguages/Create")]
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ResourceLanguageModel model)
        {
            try
            {
                var resourceId = model.ResourceId;
                var languageId = int.Parse(model.SelectedLanguage);

                var resourceLanguage = new ResourceLanguage()
                {
                    LanguageId = languageId,
                    ResourceId = resourceId,
                    EntryUser = _userId,
                    EntryDt = DateTime.Now
                };
                if (_context.ResourceLanguages.Any(x => x.LanguageId == languageId && x.ResourceId == resourceId))
                {
                    ViewData["Unique"] =
                        $"Resource already has this Language {await _context.ResourceLanguages.Where(x => x.Id == languageId).Select(x => x.Language).FirstAsync()}";
                    return View("Index", await BuildResourceLanguageIndexModel(model.ResourceId));
                }
                _context.ResourceLanguages.Add(resourceLanguage);
                await _context.SaveChangesAsync();

                // build new model for display
                var viewModel = await BuildResourceLanguageIndexModel(model.ResourceId);
                return View("Index", viewModel);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLanguages::Create({JsonConvert.SerializeObject(model)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // Get: Resource/DeleteResourceLanguage/5/6
        /// <summary>
        /// Loads the Delete Resource Language Confirmation page
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Delete Confirmation page</returns>
        [Route("ResourceLanguages/Delete/{resourceId}/{languageId}")]
        [HttpGet("Delete/{resourceId}/{languageId}")]
        public async Task<IActionResult> Delete(int? resourceId, int? languageId)
        {
            try
            {
                if (resourceId == null || languageId == null)
                {
                    return NotFound();
                }

                var language = await _context.Languages.Where(x => x.Id == languageId).FirstOrDefaultAsync();
                // build model for View
                if (language == null) return NotFound();
                {
                    var model = new ResourceLanguageDeleteModel
                    {
                        LanguageId = languageId.Value,
                        ResourceId = resourceId.Value,
                        ResourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                            .FirstOrDefaultAsync(),
                        Language = language.Language1,
                        IsAdmin = _adService.IsAdmin()
                    };
                    return View(model);
                }

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLanguages::Delete({resourceId}):{languageId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // POST: Resource/DeleteResourceLanguageConfirmed/5/6
        /// <summary>
        /// Logs the original ResourceLanguage and then deletes it
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="languageId">Language Id</param>
        /// <returns>Back to the Resource List</returns>
        [Route("ResourceLanguages/DeleteConfirmed/{resourceId}/{languageId}")]
        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int resourceId, int languageId)
        {
            try
            {
                var providerSpecialty = await _context.ResourceLanguages.Where(x => x.ResourceId == resourceId && x.LanguageId == languageId).FirstOrDefaultAsync();
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(providerSpecialty)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                _context.ResourceLanguages.Remove(providerSpecialty);
                await _context.SaveChangesAsync();
                var resourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                    .FirstOrDefaultAsync();
                ViewData["Success"] = $"Language successfully removed from Resource {resourceName}";
                return RedirectToAction("Index", "ResourceLanguages", new {id=resourceId});
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLocations::DeleteConfirmed({resourceId}:{languageId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Create Instance of LoggerService
        /// </summary>
        /// <returns>instance of logger service</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("ResourceLanguages");
        }
    }
}
