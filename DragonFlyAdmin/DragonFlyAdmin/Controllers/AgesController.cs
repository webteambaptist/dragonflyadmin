﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.Ages;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove/edit functions for Ages
    /// </summary>
    public class AgesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        public AgesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Displays the initial Index page for Ages
        /// </summary>
        /// <returns>Index view with list of Ages</returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                var model = new AgeIndexModel();

                var ages = await _context.Ages.ToListAsync();
                var ageList = ages.Select(age => new AgeView
                    {
                        Id = age.Id,
                        Age = age.Age1,
                        EntryDt = age.EntryDt,
                        EntryUser = age.EntryUser,
                        UpdateDt = age.UpdateDt,
                        UpdateUser = age.UpdateUser
                    })
                    .ToList();
                model.AgeRangeList = ageList;
                model.IsAdmin = _adService.IsAdmin();
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception Ages::Index() :: {e.Message} ");
                return RedirectToAction("Error", "Home");
            }
        }

        /// <summary>
        /// Gets the Details of the Age
        /// </summary>
        /// <param name="id">selected age</param>
        /// <returns>Age View</returns>
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var age = await _context.Ages
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (age == null)
                {
                    return NotFound();
                }

                return View(BuildAgeView(age));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Ages::Details({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Builds the Model for the Ages View
        /// </summary>
        /// <param name="age">Age to View</param>
        /// <returns>AgeView Model</returns>
        private AgeView BuildAgeView(Age age)
        {
           return new AgeView()
            {
                Id = age.Id,
                Age = age.Age1,
                EntryDt = age.EntryDt,
                EntryUser = age.EntryUser,
                UpdateDt = age.UpdateDt,
                UpdateUser = age.UpdateUser
            };

        }
        /// <summary>
        /// Loads the Create Page with Model
        /// </summary>
        /// <returns>Model to the Create new Age Screen</returns>
        public IActionResult Create()
        {

            var age = new AgeView()
            {
                EntryDt = DateTime.Now,
                EntryUser = _userId
            };
            return View(age);
        }
        /// <summary>
        /// Creates a new Age
        /// </summary>
        /// <param name="ageView">Age from Form</param>
        /// <returns>Ages Index Page</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Age,EntryUser,EntryDt")] AgeView ageView)
        {
            try
            {
                var age = new Age
                {
                    Age1 = ageView.Age,
                    EntryUser = _userId,
                    EntryDt = DateTime.Now
                };
                if (!ModelState.IsValid) return View(BuildAgeView(age));

                if (_context.Ages.Any(x => x.Age1 == ageView.Age))
                {
                    ViewData["Unique"] = $"AgeRange '{ageView.Age}' already exists.";
                    return View(BuildAgeView(age));
                }
                
                _context.Add(age);
                await _context.SaveChangesAsync();
                ViewData["Success"] = $"{ageView.Age} Successfully Created!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Ages::Create({JsonConvert.SerializeObject(ageView)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Loads the Edit Page for selected Age
        /// </summary>
        /// <param name="id">Age id</param>
        /// <returns>Edit Screen for selected Age</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var age = await _context.Ages.FindAsync(id);
                if (age == null) return NotFound();
                age.UpdateDt = DateTime.Now;
                age.UpdateUser = _userId;
                return View(BuildAgeEditModel(age));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Ages::Edit ({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
           
        }
        /// <summary>
        /// This builds the Age View model from the db Age
        /// </summary>
        /// <param name="age">db age</param>
        /// <returns>view model age</returns>
        private AgeEditModel BuildAgeEditModel(Age age)
        {
            var isMember = _adService.IsMember();
            var isAdmin = _adService.IsAdmin();
            var model = new AgeEditModel
            {
                Id = age.Id,
                Age = age.Age1,
                EntryDt = age.EntryDt,
                EntryUser = age.EntryUser,
                UpdateDt = age.UpdateDt,
                UpdateUser = age.UpdateUser,
                IsAdmin = isAdmin,
                IsMember = isMember
            };
            return model;
        }
        /// <summary>
        /// Post Edited Age to the database
        /// </summary>
        /// <param name="id">age id</param>
        /// <param name="ageEditModel">Updated Model from the form on the edit screen</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Age,EntryUser,EntryDt,UpdateUser,UpdateDt")] AgeEditModel ageEditModel)
        {
            try
            {
                if (id != ageEditModel.Id)
                {
                    return NotFound();
                }

                var model = await _context.Ages.Where(x => x.Id == id).FirstAsync();
                model.Age1 = ageEditModel.Age;
                model.UpdateDt = DateTime.Now;
                model.UpdateUser = _userId;
                if (!ModelState.IsValid) return View(BuildAgeEditModel(model));
                // if name changed and exists already
                if (model.Age1 == ageEditModel.Age && _context.Ages.Any(x => x.Age1 == ageEditModel.Age))
                {
                    ViewData["Unique"] = $"AgeRange '{ageEditModel.Age}' already exists.";
                    return View(BuildAgeEditModel(model));
                }
                try
                {
                    _context.Update(model);
                    await _context.SaveChangesAsync();
                    ViewData["Success"] = $"{ageEditModel.Age} Successfully Updated!";
                }
                catch (DbUpdateConcurrencyException eu)
                {
                    if (!AgeExists(ageEditModel.Id))
                    {
                        _logger.WriteError($"Exception :: Not Found :: Ages::Edit({id}:{JsonConvert.SerializeObject(ageEditModel)}) :: {eu.Message}");
                        return NotFound();
                    }
                    _logger.WriteError($"Exception :: DbUpdateConcurrencyException :: Ages::Edit({id}::{JsonConvert.SerializeObject(ageEditModel)}) :: {eu.Message}");
                    return RedirectToAction("Error", "Home");
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Ages::Edit({id}:{JsonConvert.SerializeObject(ageEditModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
       /// <summary>
       /// Displays the delete screen for selected age
       /// </summary>
       /// <param name="id">Age id</param>
       /// <returns>Delete View</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var age = await _context.Ages
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (age == null)
                {
                    return NotFound();
                }
                return View(BuildAgeEditModel(age));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Ages::Delete({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Adds deleted age to DeleteLog and then deletes Age
        /// </summary>
        /// <param name="id">age Id</param>
        /// <returns>Index page of Ages</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var age = await _context.Ages.FindAsync(id);
                var ageJson = JsonConvert.SerializeObject(age);
                var deleteLog = new DeleteLog
                {
                    DeleteUser = _userId,
                    JsonModel = ageJson
                };
                await _context.DeleteLogs.AddAsync(deleteLog);
                await _context.SaveChangesAsync();

                if (age != null) _context.Ages.Remove(age);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Ages::DeleteConfirmed({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Returns true/fault if age exists in the database
        /// </summary>
        /// <param name="id">age id</param>
        /// <returns>true/false</returns>
        private bool AgeExists(int id)
        {
            return _context.Ages.Any(e => e.Id == id);
        }
        /// <summary>
        /// Creates an instance of the LoggerService
        /// </summary>
        /// <returns>LoggerService instance</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Ages");
        }
    }
}
