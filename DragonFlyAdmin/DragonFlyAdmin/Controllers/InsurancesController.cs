﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.Insurances;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove/edit functions for the Insurances
    /// </summary>
    public class InsurancesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Main constructor that creates instances of the services
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public InsurancesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Main index page for Insurances
        /// </summary>
        /// <returns>Index page with list of insurances</returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                var insurances = await _context.Insurances.OrderBy(x => x.Name).ToListAsync();
                var insuranceList = insurances.Select(insurance => new Insurance()
                    {
                        Id = insurance.Id,
                        Name = insurance.Name,
                        EntryUser = insurance.EntryUser,
                        EntryDt = insurance.EntryDt,
                        UpdateUser = insurance.UpdateUser,
                        UpdateDt = insurance.UpdateDt
                    })
                    .ToList();
                var model = new InsuranceIndexModel
                {
                    InsuranceList = BuildInsuranceViews(insuranceList),
                    IsAdmin = _adService.IsAdmin()
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Insurances::Index :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// This builds the List of Insurances to display on the page
        /// </summary>
        /// <param name="insuranceList">list of insurances</param>
        /// <returns>ViewModel of Insurances List</returns>
        private List<InsuranceView> BuildInsuranceViews(IEnumerable<Insurance> insuranceList)
        {
            return insuranceList.Select(insurance => new InsuranceView
                {
                    Id = insurance.Id,
                    Name = insurance.Name,
                    EntryDt = insurance.EntryDt,
                    EntryUser = insurance.EntryUser,
                    UpdateDt = insurance.UpdateDt,
                    UpdateUser = insurance.UpdateUser
                })
                .ToList();
        }
        private InsuranceView BuildInsuranceView(Insurance insurance)
        {
            return new InsuranceView
            {
                Id = insurance.Id,
                Name = insurance.Name,
                EntryDt = insurance.EntryDt,
                EntryUser = insurance.EntryUser,
                UpdateDt = insurance.UpdateDt,
                UpdateUser = insurance.UpdateUser
            };
        }
        /// <summary>
        /// Gets the details of the selected Insurance
        /// </summary>
        /// <param name="id">Insurance Id</param>
        /// <returns>Insurance Detail View</returns>
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var insurance = await _context.Insurances
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (insurance == null)
                {
                    return NotFound();
                }

                return View(BuildInsuranceView(insurance));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Insurances::Details({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the Insurance Model for the Create new Insurance page
        /// </summary>
        /// <returns>Create Insurance View</returns>
        public IActionResult Create()
        {
            var insurance = new InsuranceView()
            {
                EntryDt = DateTime.Now,
                EntryUser = _userId
            };
            return View(insurance);
        }
        /// <summary>
        /// Creates a new Insurance and saves to the database
        /// </summary>
        /// <param name="insuranceView">Insurance Form </param>
        /// <returns>Insurance index page</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,EntryUser,EntryDt,UpdateUser,UpdateDt")] InsuranceView insuranceView)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    insuranceView.EntryDt = DateTime.Now;
                    return View(insuranceView);
                }

                if (_context.Insurances.Any(x => x.Name == insuranceView.Name))
                {
                    ViewData["Unique"] = $"AgeRange '{insuranceView.Name}' already exists.";
                    insuranceView.EntryDt = DateTime.Now;
                    return View(insuranceView);
                }
                var insurance = new Insurance
                {
                    Name = insuranceView.Name,
                    EntryUser = insuranceView.EntryUser
                };
                _context.Insurances.Add(insurance);
                await _context.SaveChangesAsync();
                ViewData["Success"] = $"{insuranceView.Name} Successfully Created!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Insurances::Create({JsonConvert.SerializeObject(insuranceView)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Displays the Edit screen for selected insurance
        /// </summary>
        /// <param name="id">Insurance Id</param>
        /// <returns>Edit page for insurance</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var insurance = await _context.Insurances.FindAsync(id);
                if (insurance == null) return NotFound();
                insurance.UpdateDt = DateTime.Now;
                insurance.UpdateUser = _userId;

                return View(BuildInsuranceEditModel(insurance));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Edit ({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Posts the changes to the Insurance
        /// </summary>
        /// <param name="id">Insurance Id</param>
        /// <param name="insuranceModel">Form</param>
        /// <returns>Index of Insurances</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,EntryUser,EntryDt,UpdateUser,UpdateDt")] InsuranceEditModel insuranceModel)
        {
            try
            {
                if (id != insuranceModel.Id)
                {
                    return NotFound();
                }

                var model = await _context.Insurances.Where(x => x.Id == id).FirstAsync();
                model.Name = insuranceModel.Name;
                model.UpdateDt = DateTime.Now;
                model.UpdateUser = _userId;

                if (!ModelState.IsValid)
                {
                    return View(BuildInsuranceEditModel(model));
                }

                if (model.Name == insuranceModel.Name && _context.Insurances.Any(x => x.Name == insuranceModel.Name))
                {
                    ViewData["Unique"] = $"Insurance '{insuranceModel.Name}' already exists.";
                    return View(BuildInsuranceEditModel(model));
                }
                try
                {
                    _context.Update(model);
                    await _context.SaveChangesAsync();
                    ViewData["Success"] = $"{insuranceModel.Name} Successfully Updated!";
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!InsuranceExists(insuranceModel.Id))
                    {
                        _logger.WriteError($"DBException :: Not Found :: Insurances::Edit({id}:{JsonConvert.SerializeObject(insuranceModel)}) :: {ex.Message}");
                        return NotFound();
                    }
                    _logger.WriteError($"DBException :: Insurances::Edit({id}:{JsonConvert.SerializeObject(insuranceModel)}) :: {ex.Message}");
                    return RedirectToAction("Error", "Home");
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Insurances::Edit({id}:{JsonConvert.SerializeObject(insuranceModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Displays the delete screen for selected Insurance
        /// </summary>
        /// <param name="id">Insurance Id</param>
        /// <returns>Delete Page</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var insurance = await _context.Insurances
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (insurance == null)
                {
                    return NotFound();
                }

                return View(BuildInsuranceEditModel(insurance));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Insurances::Delete({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Saves Insurance to DeleteLog and then deletes from database
        /// </summary>
        /// <param name="id">Insurance Id</param>
        /// <returns>Index of Insurances</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var insurance = await _context.Insurances.FindAsync(id);
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(insurance)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                if (insurance != null) _context.Insurances.Remove(insurance);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Insurances::DeleteConfirmed({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Checks if Insurance exists in the database
        /// </summary>
        /// <param name="id">Insurance Id</param>
        /// <returns>true/false</returns>
        private bool InsuranceExists(int id)
        {
            return _context.Insurances.Any(e => e.Id == id);
        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>Instance of LoggerService</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Insurances");
        }
        /// <summary>
        /// Builds the ViewModel for the Insurance
        /// </summary>
        /// <param name="insurance">db Insurance</param>
        /// <returns>ViewModel for Insurance</returns>
        private InsuranceEditModel BuildInsuranceEditModel(Insurance insurance)
        {
            var isMember = _adService.IsMember();
            var isAdmin = _adService.IsAdmin();
            var model = new InsuranceEditModel()
            {
                Id = insurance.Id,
                Name = insurance.Name,
                EntryDt = insurance.EntryDt,
                EntryUser = insurance.EntryUser,
                UpdateDt = insurance.UpdateDt,
                UpdateUser = insurance.UpdateUser,
                IsAdmin = isAdmin,
                IsMember = isMember
            };
            return model;
        }
    }
}
