﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.Languages;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove/edit for Languages
    /// </summary>
    public class LanguagesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Main constructor that creates instances of services used
        /// </summary>
        /// <param name="context">cbContext</param>
        /// <param name="accessor">Logged in user</param>
        public LanguagesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Index view that displays a list of Languages
        /// </summary>
        /// <returns>Index View</returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                var languages = await _context.Languages.OrderBy(x => x.Language1).ToListAsync();
                var languageList = languages.Select(language => new Language()
                    {
                        Id = language.Id,
                        Language1 = language.Language1,
                        EntryUser = language.EntryUser,
                        EntryDt = language.EntryDt,
                        UpdateUser = language.UpdateUser,
                        UpdateDt = language.UpdateDt
                    })
                    .ToList();
                var model = new LanguageIndexModel()
                {
                    LanguageList = BuildLanguageViews(languageList),
                    IsAdmin = _adService.IsAdmin()
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Languages::Index() :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the List of Database Languages into a list of ViewModel Languages
        /// </summary>
        /// <param name="languageList">list of db Languages</param>
        /// <returns>list of ViewModel languages</returns>
        private List<LanguageView> BuildLanguageViews(IEnumerable<Language> languageList)
        {
            return languageList.Select(language => new LanguageView()
                {
                    Id = language.Id,
                    Language = language.Language1,
                    EntryDt = language.EntryDt,
                    EntryUser = language.EntryUser,
                    UpdateDt = language.UpdateDt,
                    UpdateUser = language.UpdateUser
                })
                .ToList();
        }
        /// <summary>
        /// Builds a ViewModel Language from a Database Language
        /// </summary>
        /// <param name="language">db language</param>
        /// <returns>view model language</returns>
        private LanguageView BuildLanguageView(Language language)
        {
            return new LanguageView
            {
                Id = language.Id,
                Language = language.Language1,
                EntryDt = language.EntryDt,
                EntryUser = language.EntryUser,
                UpdateDt = language.UpdateDt,
                UpdateUser = language.UpdateUser
            };
        }
        /// <summary>
        /// Displays the details page of a Language
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>Details Page</returns>
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var language = await _context.Languages
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (language == null)
                {
                    return NotFound();
                }

                return View(BuildLanguageView(language));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Languages::Details({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Displays the Create new Language page
        /// </summary>
        /// <returns>Create Language View</returns>
        public IActionResult Create()
        {
            var language = new Language()
            {
                EntryDt = DateTime.Now,
                EntryUser = _userId
            };

            return View(BuildLanguageView(language));
        }
        /// <summary>
        /// Creates a new Language and saves it to the database
        /// </summary>
        /// <param name="languageView">form </param>
        /// <returns>Index page for Languages</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Language,EntryUser,EntryDt")] LanguageView languageView)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    languageView.EntryDt = DateTime.Now;
                    return View(languageView);
                }
                if (_context.Languages.Any(x => x.Language1 == languageView.Language))
                {
                    ViewData["Unique"] = $"Language '{languageView.Language}' already exists.";
                    languageView.EntryDt = DateTime.Now;
                    return View(languageView);
                }
                var language = new Language
                {
                    EntryUser = languageView.EntryUser,
                    Language1 = languageView.Language
                };
                _context.Languages.Add(language);
                await _context.SaveChangesAsync();
                ViewData["Success"] = $"{language.Language1} Successfully Created!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Create({JsonConvert.SerializeObject(languageView)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Displays the Edit Page
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>Edit View</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var language = await _context.Languages.FindAsync(id);
                if (language == null) return NotFound();
                language.UpdateDt = DateTime.Now;
                language.UpdateUser = _userId;
                return View(BuildLanguageEditModel(language));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Languages::Edit({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Saves the edited language to the database
        /// </summary>
        /// <param name="id">language id</param>
        /// <param name="languageModel">form</param>
        /// <returns>Index for Languages</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Language,EntryUser,EntryDt,UpdateUser,UpdateDt")] LanguageEditModel languageModel)
        {
            try
            {
                if (id != languageModel.Id)
                {
                    return NotFound();
                }
                var language = await _context.Languages.Where(x => x.Id == id).FirstAsync();
                language.Language1 = languageModel.Language;
                language.UpdateDt = DateTime.Now;
                language.UpdateUser = _userId;
                if (!ModelState.IsValid)
                {
                    return View(BuildLanguageEditModel(language));
                }
                if (language.Language1 == languageModel.Language && _context.Languages.Any(x => x.Language1 == languageModel.Language))
                {
                    ViewData["Unique"] = $"Language '{languageModel.Language}' already exists.";
                    return View(BuildLanguageEditModel(language));
                }
                try
                {
                    _context.Update(language);
                    await _context.SaveChangesAsync();
                    ViewData["Success"] = $"{languageModel.Language} Successfully Updated!";
                }
                catch (DbUpdateConcurrencyException eu)
                {
                    if (!LanguageExists(languageModel.Id))
                    {
                        _logger.WriteError($"Exception :: Not Found :: Languages::Edit({id}:{JsonConvert.SerializeObject(languageModel)}) :: {eu.Message}");
                        return NotFound();
                    }
                    _logger.WriteError($"Exception :: DbUpdateConcurrencyException :: Languages::Edit({id}:{JsonConvert.SerializeObject(languageModel)}) :: {eu.Message}");
                    return RedirectToAction("Error", "Home");
                    
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Languages::Edit({id}:{JsonConvert.SerializeObject(languageModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Displays the Language delete page
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>Delete View</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var language = await _context.Languages
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (language == null)
                {
                    return NotFound();
                }
                return View(BuildLanguageEditModel(language));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Languages::Delete({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Saves the Language to DeleteLog and then deletes it out of hte database
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>Index for Languages</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var language = await _context.Languages.FindAsync(id);
                var languageJson = JsonConvert.SerializeObject(language);
                var deleteLog = new DeleteLog()
                {
                    DeleteUser = _userId,
                    JsonModel = languageJson
                };
                await _context.DeleteLogs.AddAsync(deleteLog);
                if (language != null) _context.Languages.Remove(language);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Languages::DeleteConfirmed ({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Checks if a language exists in the database
        /// </summary>
        /// <param name="id">language id</param>
        /// <returns>true/false</returns>
        private bool LanguageExists(int id)
        {
            return _context.Languages.Any(e => e.Id == id);
        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>Instance of logger service</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Languages");
        }
        /// <summary>
        /// Creates a Language View Model based on the db language model
        /// </summary>
        /// <param name="language">db language model</param>
        /// <returns>language view model</returns>
        private LanguageEditModel BuildLanguageEditModel(Language language)
        {
            var isMember = _adService.IsMember();
            var isAdmin = _adService.IsAdmin();
            
            var model = new LanguageEditModel()
            {
                Id = language.Id,
                Language = language.Language1,
                EntryDt = language.EntryDt,
                EntryUser = language.EntryUser,
                UpdateDt = language.UpdateDt,
                UpdateUser = language.UpdateUser,
                IsAdmin = isAdmin,
                IsMember = isMember
            };
            return model;
        }
    }
}
