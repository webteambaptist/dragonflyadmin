﻿using System;
using System.Collections.Generic;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This is the main Admin Controller that directs users to the appropriate Controller
    /// based on what they select in the drop down. If they are not a dragonfly user (in ad group)
    /// They will see the unauthorized page.
    /// </summary>
    public class AdminController : Controller, ILoggerInterface
    {
        private readonly LoggerService _logger;
        private readonly AdService _adService;
        private readonly bool _isAuthorized;
        public AdminController(IHttpContextAccessor accessor)
        {
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _isAuthorized = _adService.IsAdmin() || _adService.IsMember();
        }
        // GET: AdminController
        /// <summary>
        /// Gets the Drop down for initial page load
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            try
            {
                // get a list of 
                // drop down of a list of options to add/edit/delete
                var listOptions = new List<SelectListItem>
                {
                    new SelectListItem("Ages", "Ages"),
                    new SelectListItem("Insurance", "Insurances"),
                    new SelectListItem("Location", "Locations"),
                    new SelectListItem("Resource", "Resources"),
                    new SelectListItem("Language", "Languages"),
                    new SelectListItem("Service", "Services")
                };

                var model = new MainModel
                {
                    Pages = listOptions,
                    IsAuthorized = _isAuthorized
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Admin::Index() :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Directs to appropriate Controller based on option selected
        /// </summary>
        /// <param name="model">Page Model</param>
        /// <returns>Index of selected Page</returns>
        [HttpPost]
        public ActionResult GetPage(MainModel model)
        {
            try
            {
                return model.SelectedPage switch
                {
                    "Resources" => RedirectToAction("Index", "Resources"),
                    "Locations" => RedirectToAction("Index", "Locations"),
                    "Ages" => RedirectToAction("Index", "Ages"),
                    "Insurances" => RedirectToAction("Index", "Insurances"),
                    "Languages" => RedirectToAction("Index", "Languages"),
                    "Services" => RedirectToAction("Index", "Services"),
                    _ => RedirectToAction("Error", "Home")
                };
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Admin::GetPage({JsonConvert.SerializeObject(model)}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>Instance of LoggerService</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Admin");
        }
    }
}
