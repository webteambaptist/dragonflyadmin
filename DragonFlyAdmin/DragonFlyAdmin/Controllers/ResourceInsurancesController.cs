﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.ResourceInsurances;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all the add/remove of ResourceInsurannces
    /// </summary>
    public class ResourceInsurancesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default Constructor that builds the services needed
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ResourceInsurancesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        #region ResourceInsurance
        // GET: Resource/ResourceInsuranceIndex/ResourceId
        /// <summary>
        /// Gets the Resource Insurances
        /// </summary>
        /// <param name="id">Resource Id</param>
        /// <returns>List of Resource Insurances</returns>
        [Route("ResourceInsurances/Index/{id}")]
        [HttpGet("Index/{id}")]
        public async Task<IActionResult> Index(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var model = await BuildResourceInsuranceIndexModel(id.Value);
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceInsurances::Index({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds the Model for the Resource Insurance view
        /// </summary>
        /// <param name="resourceId">Provider Id</param>
        /// <returns>Resource Insurance Model</returns>
        private async Task<ResourceInsuranceModel> BuildResourceInsuranceIndexModel(int resourceId)
        {
            try
            {
                var model = new ResourceInsuranceModel
                {
                    ResourceInsuranceList = new List<InsuranceModel>(),
                    ResourceId = resourceId
                };
                // build drop down of locations for adding
                var insurances = await _context.Insurances.OrderBy(x=>x.Name).ToListAsync();
                var listOptions = insurances.Select(insurance => new SelectListItem(insurance.Name, insurance.Id.ToString())).ToList();
                model.InsuranceList = listOptions;

                // build LocationList
                var providerInsurances = await _context.ResourceInsurances.Where(x => x.ResourceId == resourceId).ToListAsync();
                foreach (var pl in providerInsurances)
                {
                    var insurance = await _context.Insurances.Where(x => x.Id == pl.InsuranceId).FirstOrDefaultAsync();
                    var iModel = new InsuranceModel()
                    {
                        EntryUser = pl.EntryUser,
                        EntryDt = pl.EntryDt,
                        UpdateUser = pl.UpdateUser,
                        UpdateDt = pl.UpdateDt,
                        ResourceId = pl.ResourceId,
                        Insurance = insurance.Name,
                        InsuranceId = pl.InsuranceId
                    };

                    model.ResourceInsuranceList.Add(iModel);
                    model.IsAdmin = _adService.IsAdmin();
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceInsurances::BuildResourceInsuranceIndexModel({resourceId}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Adds Resource Insurance
        /// </summary>
        /// <param name="model">Resource Insurance from form</param>
        /// <returns>Back to Resource List</returns>
        [Route("ResourceInsurances/Create")]
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ResourceInsuranceModel model)
        {
            try
            {
                var resourceId = model.ResourceId;
                var insuranceId = int.Parse(model.SelectedInsurance);

                var resourceInsurance = new ResourceInsurance()
                {
                    InsuranceId = insuranceId,
                    ResourceId = resourceId,
                    EntryUser = _userId,
                    EntryDt = DateTime.Now
                };
                if (_context.ResourceInsurances.Any(x => x.InsuranceId == insuranceId && x.ResourceId == resourceId))
                {
                    ViewData["Unique"] =
                        $"Resource already has this Insurance {await _context.ResourceInsurances.Where(x => x.Id == insuranceId).Select(x => x.Insurance).FirstAsync()}";
                    return View("Index", await BuildResourceInsuranceIndexModel(model.ResourceId));
                }
                _context.ResourceInsurances.Add(resourceInsurance);
                await _context.SaveChangesAsync();

                // build new model for display
                var viewModel = await BuildResourceInsuranceIndexModel(model.ResourceId);
                return View("Index", viewModel);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceInsurances::Create({JsonConvert.SerializeObject(model)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Loads to the Delete Resource Insurance confirmation page
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="insuranceId">Insurance Id</param>
        /// <returns>Delete Confirmation View</returns>
        // Get: Provider/Delete/5/6
        [Route("ResourceInsurances/Delete/{resourceId}/{insuranceId}")]
        [HttpGet("Delete/{resourceId}/{insuranceId}")]
        public async Task<IActionResult> Delete(int? resourceId, int? insuranceId)
        {
            try
            {
                if (resourceId == null || insuranceId == null)
                {
                    return NotFound();
                }

                var insurance = await _context.Insurances.Where(x => x.Id == insuranceId).FirstOrDefaultAsync();
                // build model for View
                if (insurance == null) return NotFound();
                {
                    var model = new ResourceInsuranceDeleteModel
                    {
                        InsuranceId = insuranceId.Value,
                        ResourceId = resourceId.Value,
                        ResourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                            .FirstOrDefaultAsync(),
                        InsuranceName = insurance.Name,
                        IsAdmin = _adService.IsAdmin()
                    };
                    return View(model);
                }

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceInsurances::Delete({resourceId}):{insuranceId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // POST: Resource/Delete/5
        /// <summary>
        /// Logs the original ResourceInsurance and then removes it
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="insuranceId">Insurance Id</param>
        /// <returns>Back to the Resource List</returns>
        [Route("ResourceInsurances/DeleteConfirmed/{resourceId}/{insuranceId}")]
        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int resourceId, int insuranceId)
        {
            try
            {
                var providerInsurance = await _context.ResourceInsurances.Where(x => x.ResourceId == resourceId && x.InsuranceId == insuranceId).FirstOrDefaultAsync();
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(providerInsurance)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                _context.ResourceInsurances.Remove(providerInsurance);
                await _context.SaveChangesAsync();
                var resourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                    .FirstOrDefaultAsync();
                ViewData["Success"] = $"Resource Ages successfully removed from Resource {resourceName}";
                return RedirectToAction("Index", "ResourceInsurances", new {id=resourceId});
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceInsurances::DeleteConfirmed({resourceId}:{insuranceId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        #endregion
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>Instance of LoggerService</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("ResourceInsurances");
        }
    }
}
