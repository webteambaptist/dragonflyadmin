﻿using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.Locations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove/edit for locations
    /// </summary>
    public class LocationsController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default constructor for creating all instances of services
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public LocationsController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Main index page for Locations
        /// </summary>
        /// <returns>Index view with list of locations</returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                var locations = await _context.Locations.OrderBy(x => x.Location1).ToListAsync();
                var locationList = locations.Select(location => new Location()
                {
                    Id = location.Id,
                    Location1 = location.Location1,
                    ZipCode = location.ZipCode,
                    EntryUser = location.EntryUser,
                    EntryDate = location.EntryDate,
                    UpdateUser = location.UpdateUser,
                    UpdateDt = location.UpdateDt
                })
                    .ToList();
                var model = new LocationsIndexModel()
                {
                    LocationList = BuildLocationViews(locationList),
                    IsAdmin = _adService.IsAdmin()
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::Index :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Builds a list of ViewModel Locations from DB Model Locations
        /// </summary>
        /// <param name="locationList">db locations</param>
        /// <returns>view model locations</returns>
        private List<LocationView> BuildLocationViews(IEnumerable<Location> locationList)
        {
            return locationList.Select(location => new LocationView
            {
                Id = location.Id,
                Location = location.Location1,
                ZipCode = location.ZipCode,
                EntryDate = location.EntryDate,
                EntryUser = location.EntryUser,
                UpdateDt = location.UpdateDt,
                UpdateUser = location.UpdateUser
            })
                .ToList();
        }
        /// <summary>
        /// Builds a ViewModel Location out of a DB Model Location
        /// </summary>
        /// <param name="location">db location</param>
        /// <returns>view model location</returns>
        private LocationView BuildLocationView(Location location)
        {
            return new LocationView
            {
                Id = location.Id,
                Location = location.Location1,
                ZipCode = location.ZipCode,
                EntryDate = location.EntryDate,
                EntryUser = location.EntryUser,
                UpdateDt = location.UpdateDt,
                UpdateUser = location.UpdateUser,
                IsAdmin = _adService.IsAdmin()
            };
        }
        /// <summary>
        /// Displays the details of a location
        /// </summary>
        /// <param name="id">location id</param>
        /// <returns>Detail View</returns>
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var location = await _context.Locations
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (location == null)
                {
                    return NotFound();
                }

                return View(BuildLocationView(location));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::Details({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Displays the Create screen for adding new location
        /// </summary>
        /// <returns>Create View</returns>
        public IActionResult Create()
        {
            var location = new Location()
            {
                EntryDate = DateTime.Now,
                EntryUser = _userId
            };
            return View(BuildLocationView(location));
        }
        /// <summary>
        /// Saves a new Location to the database
        /// </summary>
        /// <param name="locationView">form</param>
        /// <returns>Index for Locations</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Location,ZipCode,EntryUser")] LocationView locationView)
        {
            try
            {
                var location = new Location
                {
                    Location1 = locationView.Location,
                    ZipCode = locationView.ZipCode,
                    EntryUser = _userId,
                    EntryDate = DateTime.Now
                };

                if (!ModelState.IsValid) return View(BuildLocationView(location));
                if (_context.Locations.Any(x => x.Location1 == locationView.Location && x.ZipCode == locationView.ZipCode))
                //if (_context.Locations.Any(x => x.Location1 == locationView.Location))
                {
                    ViewData["Unique"] = $"Location '{locationView.Location}' already exists.";
                    return View(BuildLocationView(location));
                }

                _context.Add(location);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::Create({JsonConvert.SerializeObject(locationView)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Displays the Edit screen for a location
        /// </summary>
        /// <param name="id">location id</param>
        /// <returns>Edit Location View</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var location = await _context.Locations.FindAsync(id);
                if (location == null) return NotFound();
                location.UpdateDt = DateTime.Now;
                location.UpdateUser = _userId;
                return View(BuildLocationEditModel(location));

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::Edit({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Saves the edited location to the databse
        /// </summary>
        /// <param name="id">location id</param>
        /// <param name="locationModel">form</param>
        /// <returns>Index of Locations</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Location,ZipCode,EntryUser,EntryDate,UpdateUser,UpdateDt")] LocationEditModel locationModel)
        {
            try
            {
                if (id != locationModel.Id)
                {
                    return NotFound();
                }
                var location = await _context.Locations.Where(x => x.Id == id).FirstAsync();
                location.Location1 = locationModel.Location;
                location.ZipCode = locationModel.ZipCode;
                location.UpdateDt = DateTime.Now;
                location.UpdateUser = _userId;
                if (!ModelState.IsValid) return View(BuildLocationEditModel(location));
                // if name changed and exists already
                if (location.Location1 != locationModel.Location && _context.Locations.Any(x => x.Location1 == locationModel.Location))
                {
                    ViewData["Unique"] = $"Location '{locationModel.Location}' already exists.";
                    return View(BuildLocationEditModel(location));
                }

                try
                {
                    _context.Update(location);
                    await _context.SaveChangesAsync();
                    ViewData["Success"] = $"{locationModel.Location} Successfully Updated!";
                }
                catch (DbUpdateConcurrencyException eu)
                {
                    if (!LocationExists(locationModel.Id))
                    {
                        _logger.WriteError(
                            $"DBException :: Not Found :: Locations::Edit({id}:{JsonConvert.SerializeObject(locationModel)}) :: {eu.Message}");
                        return NotFound();
                    }
                    _logger.WriteError(
                            $"DBException :: Locations::Edit({id}: {JsonConvert.SerializeObject(locationModel)}) :: {eu.Message}");
                    return RedirectToAction("Error", "Home");
                }

                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::Edit({id}:{JsonConvert.SerializeObject(locationModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Loads the delete page for a location
        /// </summary>
        /// <param name="id">location id</param>
        /// <returns>Delete View</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var location = await _context.Locations
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (location == null)
                {
                    return NotFound();
                }

                return View(BuildLocationView(location));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::Delete({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Saves the location to DeleteLog and then deletes it out of the database
        /// </summary>
        /// <param name="id">location id</param>
        /// <returns>Index Page for Locations</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var location = await _context.Locations.FindAsync(id);
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    JsonModel = JsonConvert.SerializeObject(location)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                if (location != null) _context.Locations.Remove(location);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Locations::DeleteConfirmed({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Determines if a location exists
        /// </summary>
        /// <param name="id">location id</param>
        /// <returns>true/false</returns>
        private bool LocationExists(int id)
        {
            return _context.Locations.Any(e => e.Id == id);
        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>Instance of LoggerService</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Locations");
        }
        /// <summary>
        /// Builds a VM for DB Location
        /// </summary>
        /// <param name="location">DB Location</param>
        /// <returns>VM Location</returns>
        private LocationEditModel BuildLocationEditModel(Location location)
        {
            var isMember = _adService.IsMember();
            var isAdmin = _adService.IsAdmin();
            var model = new LocationEditModel()
            {
                Id = location.Id,
                Location = location.Location1,
                ZipCode = location.ZipCode,
                EntryDate = location.EntryDate,
                EntryUser = location.EntryUser,
                UpdateDt = location.UpdateDt,
                UpdateUser = location.UpdateUser,
                IsAdmin = isAdmin,
                IsMember = isMember
            };
            return model;
        }
    }
}
