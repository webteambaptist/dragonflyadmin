﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.Services;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove/edit for Services
    /// </summary>
    public class ServicesController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default Constructor that creates instances of all services needed
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ServicesController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Builds a vm model list for Services from a db model list
        /// </summary>
        /// <param name="serviceList">list of services from db</param>
        /// <returns>vm list of services</returns>
        private List<ServiceView> BuildServiceViews(IEnumerable<Service> serviceList)
        {
            return serviceList.Select(service => new ServiceView
                {
                    Id = service.Id,
                    Service = service.Service1,
                    EntryDt = service.EntryDt,
                    EntryUser = service.EntryUser,
                    UpdateDt = service.UpdateDt,
                    UpdateUser = service.UpdateUser
                })
                .ToList();
        }
        /// <summary>
        /// Builds a vm model from a db model of Services
        /// </summary>
        /// <param name="service">db service</param>
        /// <returns>vm service</returns>
        private ServiceView BuildServiceView(Service service)
        {
            return new ServiceView
            {
                Id = service.Id,
                Service = service.Service1,
                EntryDt = service.EntryDt,
                EntryUser = service.EntryUser,
                UpdateDt = service.UpdateDt,
                UpdateUser = service.UpdateUser
            };
        }
        /// <summary>
        /// Main Index View
        /// </summary>
        /// <returns>list of Services</returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                var services = await _context.Services.OrderBy(x => x.Service1).ToListAsync();
                var servicesList = services.Select(service => new Service()
                    {
                        Id = service.Id,
                        Service1 = service.Service1,
                        EntryUser = service.EntryUser,
                        EntryDt = service.EntryDt,
                        UpdateUser = service.UpdateUser,
                        UpdateDt = service.UpdateDt
                    })
                    .ToList();
                var model = new ServicesIndexModel()
                {
                    ServiceList = BuildServiceViews(servicesList),
                    IsAdmin = _adService.IsAdmin()
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::Index() :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Displays the Service Detail Page
        /// </summary>
        /// <param name="id">service id</param>
        /// <returns>Detail View</returns>
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var service = await _context.Services
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (service == null)
                {
                    return NotFound();
                }

                return View(BuildServiceView(service));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::Details({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Displays the Create view
        /// </summary>
        /// <returns>Create View</returns>
        public IActionResult Create()
        {
            var service = new Service()
            {
                EntryDt = DateTime.Now,
                EntryUser = _userId
            };
            return View(BuildServiceView(service));
        }
        /// <summary>
        /// Creates a new Service in the DB
        /// </summary>
        /// <param name="serviceView">form</param>
        /// <returns>Services index</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Service,EntryUser,EntryDt")] ServiceView serviceView)
        {
            try
            {
                var service = new Service()
                {
                    Service1 = serviceView.Service,
                    EntryDt = DateTime.Now,
                    EntryUser = _userId
                };
                if (!ModelState.IsValid) return View(BuildServiceView(service));

                if (_context.Services.Any(x => x.Service1 == serviceView.Service))
                {
                    ViewData["Unique"] = $"Service '{serviceView.Service}' already exists.";
                    return View(BuildServiceView(service));
                }

                _context.Add(service);
                await _context.SaveChangesAsync();
                ViewData["Success"] = $"{serviceView.Service} Successfully Created!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::Create({JsonConvert.SerializeObject(serviceView)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Displays the edit page for a Service
        /// </summary>
        /// <param name="id">service id</param>
        /// <returns>Edit View</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var service = await _context.Services.FindAsync(id);
                if (service == null) return NotFound();
                service.UpdateDt = DateTime.Now;
                service.UpdateUser = _userId;

                return View(BuildServiceEditModel(service));

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::Edit({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Saves changes to Service in db
        /// </summary>
        /// <param name="id">service id</param>
        /// <param name="serviceModel">form</param>
        /// <returns>Services Index</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Service,EntryUser,EntryDt,UpdateUser,UpdateDt")] ServicesEditModel serviceModel)
        {
            try
            {
                if (id != serviceModel.Id)
                {
                    return NotFound();
                }
                var service = await _context.Services.Where(x => x.Id == id).FirstAsync();
                service.Service1 = serviceModel.Service;
                service.UpdateDt = DateTime.Now;
                service.UpdateUser = _userId;
                if (!ModelState.IsValid)
                {
                    return View(BuildServiceEditModel(service));
                }
                // if name changed and exists already
                if (service.Service1 == serviceModel.Service && _context.Services.Any(x => x.Service1 == serviceModel.Service))
                {
                    ViewData["Unique"] = $"Service '{serviceModel.Service}' already exists.";
                    return View(BuildServiceEditModel(service));
                }
                try
                {
                    _context.Update(service);
                    await _context.SaveChangesAsync();
                    ViewData["Success"] = $"{serviceModel.Service} Successfully Updated!";
                }
                catch (DbUpdateConcurrencyException eu)
                {
                    if (!ServiceExists(serviceModel.Id))
                    {
                        _logger.WriteError($"DBException :: Not Found :: Services::Edit({id}:{JsonConvert.SerializeObject(serviceModel)}) :: {eu.Message}");
                        return NotFound();
                    }
                    _logger.WriteError($"DBException :: Services::Edit({id}:{JsonConvert.SerializeObject(serviceModel)}) :: {eu.Message}");
                    return RedirectToAction("Error", "Home");
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::Edit({id}:{JsonConvert.SerializeObject(serviceModel)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Displays the Delete view for a Service
        /// </summary>
        /// <param name="id">service id</param>
        /// <returns>Delete View</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var service = await _context.Services
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (service == null)
                {
                    return NotFound();
                }

                return View(BuildServiceEditModel(service));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::Delete({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Saves record to DeleteLog and then deletes from database
        /// </summary>
        /// <param name="id">services id</param>
        /// <returns>Services Index View</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var service = await _context.Services.FindAsync(id);
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    JsonModel = JsonConvert.SerializeObject(service)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                if (service != null) _context.Services.Remove(service);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: Services::DeleteConfirmed({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
            
        }
        /// <summary>
        /// Checks if a service exists in the db
        /// </summary>
        /// <param name="id">service id</param>
        /// <returns>true/false</returns>
        private bool ServiceExists(int id)
        {
            return _context.Services.Any(e => e.Id == id);
        }
        /// <summary>
        /// Creates an instance of LoggerService
        /// </summary>
        /// <returns>instance of logger service</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Services");
        }
        /// <summary>
        /// Builds a vm for Services from a db model
        /// </summary>
        /// <param name="service">db service</param>
        /// <returns>vm service</returns>
        private ServicesEditModel BuildServiceEditModel(Service service)
        {
            var isMember = _adService.IsMember();
            var isAdmin = _adService.IsAdmin();
            var model = new ServicesEditModel()
            {
                Id = service.Id,
                Service = service.Service1,
                EntryDt = service.EntryDt,
                EntryUser = service.EntryUser,
                UpdateDt = service.UpdateDt,
                UpdateUser = service.UpdateUser,
                IsAdmin = isAdmin,
                IsMember = isMember
            };
            return model;
        }
    }
}
