﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Models;
using DragonFlyAdmin.Services;
using DragonFlyAdmin.ViewModels.ResourceLocations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// This controller handles all add/remove of ResourceLocations
    /// </summary>
    public class ResourceLocationsController : Controller, ILoggerInterface
    {
        private readonly DragonflyContext _context;
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Default Constructor that creates all the instances of services needed
        /// </summary>
        /// <param name="context">dbContext</param>
        /// <param name="accessor">logged in user</param>
        public ResourceLocationsController(DragonflyContext context, IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            _userId = _adService.CleanUsername(_adService.GetUserName());
        }
        /// <summary>
        /// Main index page to ResourceLocations
        /// </summary>
        /// <param name="id">resource id</param>
        /// <returns>list of resource locations for resource</returns>
        [Route("ResourceLocations/Index/{id}")]
        [HttpGet("Index/{id}")]
        public async Task<IActionResult> Index(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }
                var model = await BuildResourceLocationIndexModel(id.Value);
                return View(model);

            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLocations::Index({id}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Adds a new Location for the Provider
        /// </summary>
        /// <param name="model">Provider Location Model from form</param>
        /// <returns>Edit View</returns>
        [Route("ResourceLocations/Create")]
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ResourceLocationModel model)
        {
            try
            {
                var resourceId = model.ResourceId;
                var locationId = int.Parse(model.SelectedLocation);

                var resourceLocation = new ResourceLocation()
                {
                    LocationId = locationId,
                    ResourceId = resourceId,
                    EntryUser = _userId,
                    EntryDt = DateTime.Now
                };
                if (_context.ResourceLocations.Any(x => x.LocationId == locationId && x.ResourceId == resourceId))
                {
                    ViewData["Unique"] =
                        $"Resource already has this Location {await _context.ResourceLocations.Where(x => x.Id == locationId).Select(x => x.Location).FirstAsync()}";
                    return View("Index", await BuildResourceLocationIndexModel(model.ResourceId));
                }
                _context.ResourceLocations.Add(resourceLocation);
                await _context.SaveChangesAsync();

                // build new model for display
                var viewModel = await BuildResourceLocationIndexModel(model.ResourceId);
                return View("Index", viewModel);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLocations::Create({JsonConvert.SerializeObject(model)}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        /// <summary>
        /// Loads a Delete Resource Location Confirmation View
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="locationId">Location Id</param>
        /// <returns>View that Ask for confirmation of delete</returns>
        [Route("ResourceLocations/Delete/{resourceId}/{locationId}")]
        [HttpGet("Delete/{resourceId}/{locationId}")]
        public async Task<IActionResult> Delete(int? resourceId, int? locationId)
        {
            try
            {
                if (resourceId == null || locationId == null)
                {
                    return NotFound();
                }
                // build model for View
                var model = new ResourceLocationDeleteModel
                {
                    LocationId = locationId.Value,
                    ResourceId = resourceId.Value,
                    ZipCode = await _context.Locations.Where(x => x.Id == locationId).Select(x => x.ZipCode)
                        .FirstOrDefaultAsync(),
                    Name = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                        .FirstOrDefaultAsync(),
                    LocationName = await _context.Locations.Where(x => x.Id == locationId).Select(x => x.Location1)
                        .FirstOrDefaultAsync(),
                    IsAdmin = _adService.IsAdmin()
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLocations::Delete({resourceId}:{locationId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }
        }
        // POST: Provider/Delete/5/5
        /// <summary>
        /// Deletes the Resource Location 
        /// </summary>
        /// <param name="resourceId">Resource Id</param>
        /// <param name="locationId">Location Id</param>
        /// <returns>Back to the location list</returns>
        [Route("ResourceLocations/DeleteConfirmed/{resourceId}/{locationId}")]
        [HttpPost, ActionName("DeleteConfirmed")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int resourceId, int locationId)
        {
            try
            {
                var providerLocation = await _context.ResourceLocations.Where(x => x.ResourceId == resourceId && x.LocationId == locationId).FirstOrDefaultAsync();
                var log = new DeleteLog
                {
                    DeleteUser = _userId,
                    EntryDt = DateTime.Now,
                    JsonModel = JsonConvert.SerializeObject(providerLocation)
                };
                _context.DeleteLogs.Add(log);
                await _context.SaveChangesAsync();

                _context.ResourceLocations.Remove(providerLocation);
                await _context.SaveChangesAsync();
                var resourceName = await _context.Resources.Where(x => x.Id == resourceId).Select(x => x.Name)
                    .FirstOrDefaultAsync();
                ViewData["Success"] = $"Location successfully removed from Resource {resourceName}";
                return RedirectToAction("Index", "ResourceLocations", new {id=resourceId});
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLocations::DeleteConfirmed({resourceId}:{locationId}) :: {e.Message}");
                return RedirectToAction("Error", "Home");
            }

        }
        /// <summary>
        /// Builds the ResourceLocation Model
        /// </summary>
        /// <param name="resourceId">ResourceId Id</param>
        /// <returns>ResourceLocationModel</returns>
        private async Task<ResourceLocationModel> BuildResourceLocationIndexModel(int resourceId)
        {
            try
            {
                var model = new ResourceLocationModel
                {
                    LocationModelList = new List<LocationModel>(),
                    ResourceId = resourceId
                };
                // build drop down of locations for adding
                var locations = await _context.Locations.OrderBy(x=>x.Location1).ToListAsync();
                var listOptions = locations.Select(location => new SelectListItem(location.Location1 + ", " + location.ZipCode, location.Id.ToString())).ToList();
                model.LocationList = listOptions;

                var resourceLocations = await _context.ResourceLocations.Where(x => x.ResourceId == resourceId).ToListAsync();
                foreach (var pl in resourceLocations)
                {
                    var lModel = new LocationModel()
                    {
                        EntryUser = pl.EntryUser,
                        EntryDt = pl.EntryDt,
                        UpdateUser = pl.UpdateUser,
                        UpdateDt = pl.UpdateDt,
                        ResourceId = pl.ResourceId,
                        Location = await _context.Locations.Where(x => x.Id == pl.LocationId).Select(x => x.Location1).FirstOrDefaultAsync(),
                        LocationId = pl.LocationId,
                        ZipCode = _context.Locations.Where(x=>x.Id==pl.LocationId).Select(x=>x.ZipCode).FirstOrDefault()
                    };

                    model.LocationModelList.Add(lModel);
                    model.IsAdmin = _adService.IsAdmin();
                }

                return model;
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception :: ResourceLocations::BuildResourceLocationIndexModel({resourceId}) :: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// Creates an instance of the LoggerService
        /// </summary>
        /// <returns>instance of logger service</returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("ResourceLocations");
        }
    }
}
