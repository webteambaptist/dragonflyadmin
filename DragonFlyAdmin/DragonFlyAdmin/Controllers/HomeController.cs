﻿using Microsoft.AspNetCore.Mvc;
using DragonFlyAdmin.Interfaces;
using DragonFlyAdmin.Services;
using Microsoft.AspNetCore.Http;

namespace DragonFlyAdmin.Controllers
{
    /// <summary>
    /// Main Home Controller that determines if a user is authorized before they get to Admin
    /// </summary>
    public class HomeController : Controller, ILoggerInterface
    {
        private readonly LoggerService _logger;
        private readonly string _userId;
        private readonly AdService _adService;
        /// <summary>
        /// Main Constructor that creates an instance of the services needed
        /// </summary>
        /// <param name="accessor"></param>
        public HomeController(IHttpContextAccessor accessor)
        {
            _logger = CreateLogger();
            _adService = new AdService(accessor);
            var user = _adService.GetUserName();
            _userId = _adService.CleanUsername(user);
        }
        /// <summary>
        /// Displays main Index page ONLY if they are authorized
        /// </summary>
        /// <returns>Main Index page that displays a drop down of pages</returns>
        public IActionResult Index()
        {
            var isAuthorized = _adService.IsAdmin() || _adService.IsMember();
            // make sure users are in one of the AD Groups
            // need to add the new groups as well
            return !isAuthorized ? View("NoAccess") : RedirectToAction("Index", "Admin");
        }
        /// <summary>
        /// Not used: Privacy
        /// </summary>
        /// <returns></returns>
        public IActionResult Privacy()
        {
            return View();
        }
        /// <summary>
        /// Error Page
        /// </summary>
        /// <returns></returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View();
        }
        /// <summary>
        /// Creates instance of LoggerService
        /// </summary>
        /// <returns></returns>
        public LoggerService CreateLogger()
        {
            return new LoggerService("Home");
        }
    }
}
