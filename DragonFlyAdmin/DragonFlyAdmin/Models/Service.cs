﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class Service
    {
        public Service()
        {
            ResourceServices = new HashSet<ResourceService>();
        }

        public int Id { get; set; }
        public string Service1 { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual ICollection<ResourceService> ResourceServices { get; set; }
    }
}
