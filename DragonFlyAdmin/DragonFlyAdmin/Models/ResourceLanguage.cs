﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class ResourceLanguage
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public int ResourceId { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual Language Language { get; set; }
        public virtual Resource Resource { get; set; }
    }
}
