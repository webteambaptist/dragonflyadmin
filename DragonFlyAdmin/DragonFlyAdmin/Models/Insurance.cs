﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class Insurance
    {
        public Insurance()
        {
            ResourceInsurances = new HashSet<ResourceInsurance>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual ICollection<ResourceInsurance> ResourceInsurances { get; set; }
    }
}
