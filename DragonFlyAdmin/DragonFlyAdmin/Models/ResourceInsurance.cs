﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class ResourceInsurance
    {
        public int Id { get; set; }
        public int InsuranceId { get; set; }
        public int ResourceId { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual Insurance Insurance { get; set; }
        public virtual Resource Resource { get; set; }
    }
}
