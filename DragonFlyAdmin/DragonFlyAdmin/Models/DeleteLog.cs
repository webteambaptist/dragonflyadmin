﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class DeleteLog
    {
        public int Id { get; set; }
        public string JsonModel { get; set; }
        public string DeleteUser { get; set; }
        public DateTime EntryDt { get; set; }
    }
}
