﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class Resource
    {
        public Resource()
        {
            ResourceAges = new HashSet<ResourceAge>();
            ResourceInsurances = new HashSet<ResourceInsurance>();
            ResourceLanguages = new HashSet<ResourceLanguage>();
            ResourceLocations = new HashSet<ResourceLocation>();
            ResourceServices = new HashSet<ResourceService>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual ICollection<ResourceAge> ResourceAges { get; set; }
        public virtual ICollection<ResourceInsurance> ResourceInsurances { get; set; }
        public virtual ICollection<ResourceLanguage> ResourceLanguages { get; set; }
        public virtual ICollection<ResourceLocation> ResourceLocations { get; set; }
        public virtual ICollection<ResourceService> ResourceServices { get; set; }
    }
}
