﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class DragonflyContext : DbContext
    {
        public DragonflyContext()
        {
        }

        public DragonflyContext(DbContextOptions<DragonflyContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Age> Ages { get; set; }
        public virtual DbSet<DeleteLog> DeleteLogs { get; set; }
        public virtual DbSet<Insurance> Insurances { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<ResourceAge> ResourceAges { get; set; }
        public virtual DbSet<ResourceInsurance> ResourceInsurances { get; set; }
        public virtual DbSet<ResourceLanguage> ResourceLanguages { get; set; }
        public virtual DbSet<ResourceLocation> ResourceLocations { get; set; }
        public virtual DbSet<ResourceService> ResourceServices { get; set; }
        public virtual DbSet<Service> Services { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Age>(entity =>
            {
                entity.ToTable("Ages", "dbo");

                entity.HasIndex(e => e.Age1, "UC_Ages")
                    .IsUnique();

                entity.Property(e => e.Age1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Age");

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);
            });

            modelBuilder.Entity<DeleteLog>(entity =>
            {
                entity.ToTable("DeleteLog", "dbo");

                entity.Property(e => e.DeleteUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.JsonModel).IsRequired();
            });

            modelBuilder.Entity<Insurance>(entity =>
            {
                entity.ToTable("Insurances", "dbo");

                entity.HasIndex(e => e.Name, "uc_insurance")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.ToTable("Languages", "dbo");

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Language1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("Language");

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.ToTable("Locations", "dbo");

                entity.HasIndex(e => new { e.Location1, e.ZipCode }, "uc_Location")
                    .IsUnique();

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Location1)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("Location");

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Resource>(entity =>
            {
                entity.ToTable("Resources", "dbo");

                entity.HasIndex(e => e.Name, "uc_resources")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.Property(e => e.Website).IsRequired();
            });

            modelBuilder.Entity<ResourceAge>(entity =>
            {
                entity.ToTable("ResourceAges", "dbo");

                entity.HasIndex(e => new { e.AgeId, e.ResourceId }, "uc_resage")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.HasOne(d => d.Age)
                    .WithMany(p => p.ResourceAges)
                    .HasForeignKey(d => d.AgeId)
                    .HasConstraintName("FK_ResourceAges_Ages");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ResourceAges)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ResourceAges_Resources");
            });

            modelBuilder.Entity<ResourceInsurance>(entity =>
            {
                entity.ToTable("ResourceInsurances", "dbo");

                entity.HasIndex(e => new { e.InsuranceId, e.ResourceId }, "uc_recins")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.ResourceInsurances)
                    .HasForeignKey(d => d.InsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ResourceInsurances_Insurances");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ResourceInsurances)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ResourceInsurances_Resources");
            });

            modelBuilder.Entity<ResourceLanguage>(entity =>
            {
                entity.ToTable("ResourceLanguages", "dbo");

                entity.HasIndex(e => new { e.LanguageId, e.ResourceId }, "uc_resclang")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.HasOne(d => d.Language)
                    .WithMany(p => p.ResourceLanguages)
                    .HasForeignKey(d => d.LanguageId)
                    .HasConstraintName("FK_ResourceLanguages_Languages");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ResourceLanguages)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ResourceLanguages_Resources");
            });

            modelBuilder.Entity<ResourceLocation>(entity =>
            {
                entity.ToTable("ResourceLocations", "dbo");

                entity.HasIndex(e => new { e.LocationId, e.ResourceId }, "uc_rescLoc")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.ResourceLocations)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_ResourceLocations_Locations");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ResourceLocations)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ResourceLocations_Resources");
            });

            modelBuilder.Entity<ResourceService>(entity =>
            {
                entity.ToTable("ResourceServices", "dbo");

                entity.HasIndex(e => new { e.ServicesId, e.ResourceId }, "uc_rescserv")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ResourceServices)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_ResourceServices_Resources");

                entity.HasOne(d => d.Services)
                    .WithMany(p => p.ResourceServices)
                    .HasForeignKey(d => d.ServicesId)
                    .HasConstraintName("FK_ResourceServices_Services");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("Services", "dbo");

                entity.HasIndex(e => e.Service1, "uc_Service")
                    .IsUnique();

                entity.Property(e => e.EntryDt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EntryUser)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Service1)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("Service");

                entity.Property(e => e.UpdateDt).HasColumnType("datetime");

                entity.Property(e => e.UpdateUser).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
