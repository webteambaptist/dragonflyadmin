﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class Language
    {
        public Language()
        {
            ResourceLanguages = new HashSet<ResourceLanguage>();
        }

        public int Id { get; set; }
        public string Language1 { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual ICollection<ResourceLanguage> ResourceLanguages { get; set; }
    }
}
