﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class ResourceService
    {
        public int Id { get; set; }
        public int ServicesId { get; set; }
        public int ResourceId { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual Resource Resource { get; set; }
        public virtual Service Services { get; set; }
    }
}
