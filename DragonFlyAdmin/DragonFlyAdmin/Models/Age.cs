﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class Age
    {
        public Age()
        {
            ResourceAges = new HashSet<ResourceAge>();
        }

        public int Id { get; set; }
        public string Age1 { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual ICollection<ResourceAge> ResourceAges { get; set; }
    }
}
