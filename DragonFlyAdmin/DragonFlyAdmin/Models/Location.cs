﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class Location
    {
        public Location()
        {
            ResourceLocations = new HashSet<ResourceLocation>();
        }

        public int Id { get; set; }
        public string Location1 { get; set; }
        public string ZipCode { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual ICollection<ResourceLocation> ResourceLocations { get; set; }
    }
}
