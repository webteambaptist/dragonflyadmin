﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DragonFlyAdmin.Models
{
    public partial class ResourceAge
    {
        public int Id { get; set; }
        public int AgeId { get; set; }
        public int ResourceId { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }

        public virtual Age Age { get; set; }
        public virtual Resource Resource { get; set; }
    }
}
