﻿namespace DragonFlyAdmin.ViewModels.ResourceLocations
{
    public class ResourceLocationDeleteModel
    {
        public string Name { get; set; }
        public string ZipCode { get; set; }
        public string LocationName { get; set; }
        public int ResourceId { get; set; }
        public int LocationId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
