﻿using System;
using System.Collections.Generic;
using DragonFlyAdmin.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels.ResourceLocations
{
    public class ResourceLocationModel
    {
        public int ResourceId { get; set; }
        public List<LocationModel> LocationModelList { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public string SelectedLocation { get; set; }
        public bool IsAdmin { get; set; }
    }
    public partial class LocationModel
    {
        public int LocationId { get; set; }
        public int ResourceId { get; set; }
        public string Location { get; set; }
        public string ZipCode { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
}
    
