﻿using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Languages
{
    public class LanguageEditModel : LanguageView
    {
        public bool IsAdmin { get; set; }
        public bool IsMember { get; set; }
    }
}
