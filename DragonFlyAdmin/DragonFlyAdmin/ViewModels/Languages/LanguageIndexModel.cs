﻿using System.Collections.Generic;
using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Languages
{
    public class LanguageIndexModel : LanguageView
    {
        public List<LanguageView> LanguageList { get; set; }
        public bool IsAdmin { get; set; }
    }
}
