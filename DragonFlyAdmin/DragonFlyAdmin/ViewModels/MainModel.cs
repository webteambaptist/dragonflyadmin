﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels
{
    public class MainModel
    {
        public List<SelectListItem> Pages { get; set; }
        public string SelectedPage { get; set; }
        public bool IsAuthorized { get; set; }
    }
}
