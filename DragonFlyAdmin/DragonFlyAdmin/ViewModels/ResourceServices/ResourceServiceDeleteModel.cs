﻿namespace DragonFlyAdmin.ViewModels.ResourceServices
{
    public class ResourceServiceDeleteModel
    {

        public string ResourceName { get; set; }
        public string ServiceName { get; set; }
        public int ResourceId { get; set; }
        public int ServiceId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
