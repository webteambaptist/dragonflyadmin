﻿using System;
using System.Collections.Generic;
using DragonFlyAdmin.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels.ResourceServices
{
    public class ResourceServiceModel
    {
        public int ResourceId { get; set; }
        public List<ServiceModel> ServiceModelList { get; set; }
        public List<SelectListItem> ServiceList { get; set; }
        public string SelectedService { get; set; }
        public bool IsAdmin { get; set; }
    }
    public partial class ServiceModel
    {
        public int ServiceId { get; set; }
        public int ResourceId { get; set; }
        public string Service { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
}
    
