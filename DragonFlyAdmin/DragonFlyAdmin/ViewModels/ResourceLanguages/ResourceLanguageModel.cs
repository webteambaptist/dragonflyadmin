﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels.ResourceLanguages
{
    public class ResourceLanguageModel
    {
        public int ResourceId { get; set; }
        public List<LanguageModel> LanguageModelList { get; set; }
        public List<SelectListItem> LanguageList { get; set; }
        public string SelectedLanguage { get; set; }
        public bool IsAdmin { get; set; }
    }
    public partial class LanguageModel
    {
        public int LanguageId { get; set; }
        public int ResourceId { get; set; }
        public string Language { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
}
