﻿namespace DragonFlyAdmin.ViewModels.ResourceLanguages
{
    public class ResourceLanguageDeleteModel
    {
        public string ResourceName { get; set; }
        public string Language { get; set; }
        public int ResourceId { get; set; }
        public int LanguageId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
