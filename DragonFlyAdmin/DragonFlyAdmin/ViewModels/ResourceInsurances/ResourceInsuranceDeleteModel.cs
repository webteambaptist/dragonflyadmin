﻿namespace DragonFlyAdmin.ViewModels.ResourceInsurances
{
    public class ResourceInsuranceDeleteModel
    {
        public string ResourceName { get; set; }
        public string InsuranceName { get; set; }
        public int ResourceId { get; set; }
        public int InsuranceId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
