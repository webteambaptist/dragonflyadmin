﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels.ResourceInsurances
{
    public class ResourceInsuranceModel
    {
        public int ResourceId { get; set; }
        public List<InsuranceModel> ResourceInsuranceList { get; set; }
        public List<SelectListItem> InsuranceList { get; set; }
        public string SelectedInsurance { get; set; }
        public bool IsAdmin { get; set; }
    }
    public partial class InsuranceModel
    {
        public int InsuranceId { get; set; }
        public int ResourceId { get; set; }
        public string Insurance { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
}
