﻿using System.Collections.Generic;

namespace DragonFlyAdmin.ViewModels.Resources
{
    public class ResourceIndexModel
    {
        public List<ResourceView> ResourceList { get; set; }
        public bool IsAdmin { get; set; }
    }
}
