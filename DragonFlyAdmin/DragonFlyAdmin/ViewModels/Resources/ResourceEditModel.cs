﻿using System.Collections.Generic;
using DragonFlyAdmin.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels.Resources
{
    public class ResourceEditModel : ResourceView
    {
        public bool IsAdmin { get; set; }
        public bool IsMember { get; set; }
        //public List<SelectListItem> Ages { get; set; }
    }
}
