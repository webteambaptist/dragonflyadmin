﻿using System.Collections.Generic;
using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Ages
{
    public class AgeIndexModel
    {
        public List<AgeView> AgeRangeList { get; set; }
        public bool IsAdmin { get; set; }
    }
}
