﻿using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Ages
{
    public class AgeEditModel : AgeView
    {
        public bool IsAdmin { get; set; }
        public bool IsMember { get; set; }
    }
}
