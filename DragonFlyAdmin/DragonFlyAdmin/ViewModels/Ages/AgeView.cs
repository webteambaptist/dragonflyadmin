﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace DragonFlyAdmin.ViewModels.Ages
{
    public partial class AgeView
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Age is Required")]
        [MaxLength(50)]
        public string Age { get; set; }
        [DisplayName("Entry User")]
        [MaxLength(100)]
        public string EntryUser { get; set; }
        [DisplayName("Entry Date")]
        [DataType(DataType.DateTime)]
        public DateTime EntryDt { get; set; }
        [DisplayName("Update User")]
        [MaxLength(100)]
        public string UpdateUser { get; set; }
        [DisplayName("Update Date")]
        [DataType(DataType.DateTime)]
        public DateTime? UpdateDt { get; set; }
    }
}
