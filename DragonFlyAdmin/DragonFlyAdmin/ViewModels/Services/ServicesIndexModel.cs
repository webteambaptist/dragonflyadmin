﻿using System.Collections.Generic;
using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Services
{
    public class ServicesIndexModel : ServiceView
    {
        public List<ServiceView> ServiceList { get; set; }
        public bool IsAdmin { get; set; }
    }
}
