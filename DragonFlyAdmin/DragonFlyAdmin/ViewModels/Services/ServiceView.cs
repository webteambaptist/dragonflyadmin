﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace DragonFlyAdmin.ViewModels.Services
{
    public partial class ServiceView
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Service Name is required")]
        [DisplayName("Service Name")]
        public string Service { get; set; }
        [DisplayName("Entry User")]
        [MaxLength(100)]
        public string EntryUser { get; set; }
        [DisplayName("Entry Date")]
        [DataType(DataType.DateTime)]
        public DateTime EntryDt { get; set; }
        [DisplayName("Update User")]
        [MaxLength(100)]
        public string UpdateUser { get; set; }
        [DisplayName("Update Date")]
        [DataType(DataType.DateTime)]
        public DateTime? UpdateDt { get; set; }
    }
}
