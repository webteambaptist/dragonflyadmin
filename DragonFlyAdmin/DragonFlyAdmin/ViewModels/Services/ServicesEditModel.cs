﻿using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Services
{
    public class ServicesEditModel : ServiceView
    {
        public bool IsAdmin { get; set; }
        public bool IsMember { get; set; }
    }
}
