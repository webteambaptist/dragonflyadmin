﻿using System.Collections.Generic;
using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Insurances
{
    public class InsuranceIndexModel
    {
        public List<InsuranceView> InsuranceList { get; set; }
        public bool IsAdmin { get; set; }
    }
    
}
