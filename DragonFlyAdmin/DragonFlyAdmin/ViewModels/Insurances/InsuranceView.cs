﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace DragonFlyAdmin.ViewModels.Insurances
{
    public partial class InsuranceView
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Insurance Name is Required")]
        [MaxLength(200)]
        [DisplayName("Insurance Name")]
        public string Name { get; set; }
        [DisplayName("Entry User")]
        [MaxLength(100)]
        public string EntryUser { get; set; }
        [DisplayName("Entry Date")]
        [DataType(DataType.DateTime)]
        public DateTime EntryDt { get; set; }
        [DisplayName("Update User")]
        [MaxLength(100)]
        public string UpdateUser { get; set; }
        [DisplayName("Update Date")]
        [DataType(DataType.DateTime)]
        public DateTime? UpdateDt { get; set; }
        
    }
}
