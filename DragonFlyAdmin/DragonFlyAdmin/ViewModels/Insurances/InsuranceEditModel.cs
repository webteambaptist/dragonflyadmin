﻿using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Insurances
{
    public class InsuranceEditModel : InsuranceView
    {
        public bool IsAdmin { get; set; }
        public bool IsMember { get; set; }
    }
}
