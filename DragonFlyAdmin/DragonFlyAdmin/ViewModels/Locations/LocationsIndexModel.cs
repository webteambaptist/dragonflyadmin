﻿using System.Collections.Generic;
using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Locations
{
    public class LocationsIndexModel : LocationView
    {
        public List<LocationView> LocationList { get; set; }
        public bool IsAdmin { get; set; }
    }
}
