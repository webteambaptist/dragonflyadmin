﻿using DragonFlyAdmin.Models;

namespace DragonFlyAdmin.ViewModels.Locations
{
    public class LocationEditModel : LocationView
    {
        public bool IsAdmin { get; set; }
        public bool IsMember { get; set; }
    }
}
