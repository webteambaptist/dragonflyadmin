﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace DragonFlyAdmin.ViewModels.Locations
{
    public partial class LocationView
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Location is Required")]
        [MaxLength(250)]
        public string Location { get; set; }
        [Required(ErrorMessage = "Zip Code is Required")]
        [MaxLength(100)]
        public string ZipCode { get; set; }
        [DisplayName("Entry User")]
        [MaxLength(100)]
        public string EntryUser { get; set; }
        [DisplayName("Entry Date")]
        [DataType(DataType.DateTime)]
        public DateTime EntryDate { get; set; }
        [DisplayName("Update User")]
        [MaxLength(100)]
        public string UpdateUser { get; set; }
        [DisplayName("Update Date")]
        [DataType(DataType.DateTime)]
        public DateTime? UpdateDt { get; set; }
        public bool IsAdmin { get; set; }
    }
}
