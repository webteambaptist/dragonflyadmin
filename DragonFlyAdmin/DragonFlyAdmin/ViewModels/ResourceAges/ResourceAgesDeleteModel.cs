﻿namespace DragonFlyAdmin.ViewModels.ResourceAges
{
    public class ResourceAgesDeleteModel
    {
        public string ResourceName { get; set; }
        public string AgeName { get; set; }
        public int ResourceId { get; set; }
        public int AgeId { get; set; }
        public bool IsAdmin { get; set; }
    }
}
