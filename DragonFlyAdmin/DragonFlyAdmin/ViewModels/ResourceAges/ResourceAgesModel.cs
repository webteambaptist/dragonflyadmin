﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DragonFlyAdmin.ViewModels.ResourceAges
{
    public class ResourceAgeModel
    {
        public int ResourceId { get; set; }
        public List<AgeModel> ResourceAgesList { get; set; }
        public List<SelectListItem> AgeList { get; set; }
        public string SelectedAge { get; set; }
        public bool IsAdmin { get; set; }
    }
    public partial class AgeModel
    {
        public int AgeId { get; set; }
        public int ResourceId { get; set; }
        public string Age { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }
}
